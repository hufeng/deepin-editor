#include "test_toast.h"

test_toast::test_toast()
{

}

//void pop();
TEST_F(test_toast, pop)
{
    Toast *a = new Toast();
    a->pop();
    assert(1==1);
}
//void pack();
TEST_F(test_toast, pack)
{
    Toast *a = new Toast();
    a->pack();
    assert(1==1);
}

//void setOnlyShow(bool onlyshow);
TEST_F(test_toast, setOnlyShow)
{
    Toast *a = new Toast();
    a->setOnlyShow(true);
    assert(1==1);
}
//void setText(QString text);
TEST_F(test_toast, setText)
{
    Toast *a = new Toast();
    a->setText("true");
    assert(1==1);
}
//void setIcon(QString icon);
TEST_F(test_toast, setIcon)
{
    Toast *a = new Toast();
    a->setIcon("true");
    assert(1==1);
}

//void showAnimation();
TEST_F(test_toast, showAnimation)
{
    Toast *a = new Toast();
    a->showAnimation();
    assert(1==1);
}
//void hideAnimation();
TEST_F(test_toast, hideAnimation)
{
    Toast *a = new Toast();
    a->hideAnimation();
    assert(1==1);
}

//void setReloadState(bool enable);
TEST_F(test_toast, setReloadState)
{
    Toast *a = new Toast();
    a->setReloadState(false);
    a->setReloadState(true);
    assert(1==1);
}

//signals:
//void visibleChanged(bool visible);
//void reloadBtnClicked();
//void closeBtnClicked();
//void saveAsBtnClicked();

//private:
//void setTheme(QString theme);
TEST_F(test_toast, setTheme)
{
    Toast *a = new Toast();
    a->setTheme("dark");
    assert(1==1);
}

//qreal opacity() const;
TEST_F(test_toast, opacity)
{
    Toast *a = new Toast();
    a->opacity();
    assert(1==1);
}
extern void GenerateSettingTranslate();
//void setOpacity(qreal opacity);
TEST_F(test_toast, setOpacity)
{
    qreal o =10.99;
    GenerateSettingTranslate();
    Toast *a = new Toast();
    a->setOpacity(o);
    assert(1==1);
}



//void initCloseBtn(QString theme);

//void showEvent(QShowEvent *);
//void hideEvent(QHideEvent *);

