<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ms">
<context>
    <name>BottomBar</name>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="38"/>
        <source>Row</source>
        <translation>Baris</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="39"/>
        <source>Column</source>
        <translation>Lajur</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="40"/>
        <source>Characters %1</source>
        <translation>Aksara %1</translation>
    </message>
</context>
<context>
    <name>DDropdownMenu</name>
    <message>
        <location filename="../src/widgets/ddropdownmenu.cpp" line="271"/>
        <location filename="../src/widgets/ddropdownmenu.cpp" line="321"/>
        <source>None</source>
        <translation type="unfinished">Tiada</translation>
    </message>
</context>
<context>
    <name>EditWrapper</name>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="533"/>
        <source>File has changed on disk. Reload?</source>
        <translation>Fail telah berubah di dalam cakera. Muat semula?</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="530"/>
        <source>File removed on the disk. Save it now?</source>
        <translation>Fail dalam cakera telah dibuang. Simpan ia sekarang?</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="273"/>
        <source>Do you want to save this file?</source>
        <translation type="unfinished">Anda mahu menyimpan fail ini?</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="231"/>
        <location filename="../src/editor/editwrapper.cpp" line="277"/>
        <source>Discard</source>
        <translation type="unfinished">Singkir</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="227"/>
        <source>Encoding changed. Do you want to save the file now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="555"/>
        <location filename="../src/widgets/bottombar.cpp" line="60"/>
        <source>INSERT</source>
        <translation>SISIP</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="558"/>
        <source>OVERWRITE</source>
        <translation>TULISGANTI</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="561"/>
        <source>R/O</source>
        <translation>R/O</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="230"/>
        <location filename="../src/editor/editwrapper.cpp" line="276"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="174"/>
        <location filename="../src/editor/editwrapper.cpp" line="232"/>
        <location filename="../src/editor/editwrapper.cpp" line="278"/>
        <location filename="../src/editor/editwrapper.cpp" line="463"/>
        <source>Save</source>
        <translation>Simpan</translation>
    </message>
</context>
<context>
    <name>FindBar</name>
    <message>
        <location filename="../src/controls/findbar.cpp" line="40"/>
        <source>Find</source>
        <translation>Cari</translation>
    </message>
    <message>
        <location filename="../src/controls/findbar.cpp" line="46"/>
        <source>Next</source>
        <translation>Berikutnya</translation>
    </message>
    <message>
        <location filename="../src/controls/findbar.cpp" line="44"/>
        <source>Previous</source>
        <translation>Terdahulu</translation>
    </message>
</context>
<context>
    <name>JumpLineBar</name>
    <message>
        <location filename="../src/controls/jumplinebar.cpp" line="41"/>
        <source>Go to Line: </source>
        <translation>Pergi ke Baris:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/editorapplication.cpp" line="7"/>
        <source>Text Editor is a powerful tool for viewing and editing text files.</source>
        <translation>Penyunting Teks ialah sebuah alat melihat dan menyunting fail teks yang hebat.</translation>
    </message>
    <message>
        <location filename="../src/editorapplication.cpp" line="18"/>
        <source>Text Editor</source>
        <translation>Penyunting Teks</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/startmanager.cpp" line="718"/>
        <source>File not saved</source>
        <translation>Fail tidak disimpan</translation>
    </message>
    <message>
        <location filename="../src/editorapplication.cpp" line="15"/>
        <source>Text Editor</source>
        <translation>Penyunting Teks</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="24"/>
        <source>Basic</source>
        <translation>Asas</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="25"/>
        <source>Font Style</source>
        <translation>Gaya Fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="26"/>
        <source>Font</source>
        <translation>Fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="27"/>
        <source>Font Size</source>
        <translation>Saiz Fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="29"/>
        <location filename="../src/controls/settingsdialog.cpp" line="30"/>
        <source>Keymap</source>
        <translation>Peta Kunci</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="28"/>
        <source>Shortcuts</source>
        <translation>Pintasan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="58"/>
        <source>Forward character</source>
        <translation>Aksara maju</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="59"/>
        <source>Backward character</source>
        <translation>Aksara undur</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="92"/>
        <source>Transpose character</source>
        <translation>Transposisi aksara</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="109"/>
        <source>Tab width</source>
        <translation>Lebar tab</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="111"/>
        <source>Word wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="31"/>
        <location filename="../src/controls/settingsdialog.cpp" line="107"/>
        <location filename="../src/widgets/window.cpp" line="1386"/>
        <source>Window</source>
        <translation>Tetingkap</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="32"/>
        <source>New tab</source>
        <translation>Tab baharu</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="33"/>
        <source>New window</source>
        <translation>Tetingkap baharu</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="34"/>
        <source>Save</source>
        <translation>Simpan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="35"/>
        <source>Save as</source>
        <translation>Simpan sebagai</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="36"/>
        <source>Next tab</source>
        <translation>Tab berikutnya</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="37"/>
        <source>Previous tab</source>
        <translation>Tab terdahulu</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="38"/>
        <source>Close tab</source>
        <translation>Tutup tab</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="39"/>
        <source>Close other tabs</source>
        <translation>Tutup tab lain</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="40"/>
        <source>Restore tab</source>
        <translation>Pulihkan tab</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="41"/>
        <source>Open file</source>
        <translation>Buka fail</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="42"/>
        <source>Increment font size</source>
        <translation>Tingkatkan saiz fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="43"/>
        <source>Decrement font size</source>
        <translation>Kurangkan saiz fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="44"/>
        <source>Reset font size</source>
        <translation>Tetap semula saiz fon</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="46"/>
        <source>Toggle fullscreen</source>
        <translation>Togol skrin penuh</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="45"/>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="47"/>
        <source>Find</source>
        <translation>Cari</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="48"/>
        <source>Replace</source>
        <translation>Ganti</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="49"/>
        <source>Go to line</source>
        <translation>Pergi ke baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="50"/>
        <source>Save cursor position</source>
        <translation>Simpan kedudukan kursor</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="51"/>
        <source>Reset cursor position</source>
        <translation>Tetap semula kedudukan kursor</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="52"/>
        <source>Exit</source>
        <translation>Keluar</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="53"/>
        <source>Display shortcuts</source>
        <translation>Papar pintasan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="54"/>
        <source>Print</source>
        <translation>Cetak</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="55"/>
        <source>Editor</source>
        <translation>Penyunting</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="56"/>
        <source>Increase indent</source>
        <translation>Tingkatkan inden</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="57"/>
        <source>Decrease indent</source>
        <translation>Kurangkan inden</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="60"/>
        <source>Forward word</source>
        <translation>Maju perkataan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="61"/>
        <source>Backward word</source>
        <translation>Undur perkataan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="62"/>
        <source>Next line</source>
        <translation>Baris berikutnya</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="63"/>
        <source>Previous line</source>
        <translation>Baris terdahulu</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="64"/>
        <source>New line</source>
        <translation>Baris baharu</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="65"/>
        <source>New line above</source>
        <translation>Baris baharu di atas</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="66"/>
        <source>New line below</source>
        <translation>Baris baharu di bawah</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="67"/>
        <source>Duplicate line</source>
        <translation>Gandakan baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="68"/>
        <source>Delete to end of line</source>
        <translation>Padam ke penghujung baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="69"/>
        <source>Delete current line</source>
        <translation>Padam baris semasa</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="70"/>
        <source>Swap line up</source>
        <translation>Silih baris ke atas</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="71"/>
        <source>Swap line down</source>
        <translation>Silih baris ke bawah</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="72"/>
        <source>Scroll up one line</source>
        <translation>Tatal ke atas satu baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="73"/>
        <source>Scroll down one line</source>
        <translation>Tatal ke bawah satu baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="74"/>
        <source>Page up</source>
        <translation>Halaman ke atas</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="75"/>
        <source>Page down</source>
        <translation>Halaman ke bawah</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="76"/>
        <source>Move to end of line</source>
        <translation>Gerak ke penghujung baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="77"/>
        <source>Move to start of line</source>
        <translation>Gerak ke permulaan baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="78"/>
        <source>Move to end of text</source>
        <translation>Gerak ke penghujung teks</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="79"/>
        <source>Move to start of text</source>
        <translation>Gerak ke permulaan teks</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="80"/>
        <source>Move to line indentation</source>
        <translation>Alih ke indentasi baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="81"/>
        <source>Upper case</source>
        <translation>Huruf besar</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="82"/>
        <source>Lower case</source>
        <translation>Huruf kecil</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="83"/>
        <source>Capitalize</source>
        <translation>Huruf Besarkan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="84"/>
        <source>Delete backward word</source>
        <translation>Padam perkataan mengundur</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="85"/>
        <source>Delete forward word</source>
        <translation>Padam perkataan ke hadapan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="86"/>
        <source>Forward over a pair</source>
        <translation>Majukan melangkaui pasangan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="87"/>
        <source>Backward over a pair</source>
        <translation>Undur melangkaui pasangan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="88"/>
        <source>Select all</source>
        <translation>Pilih semua</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="89"/>
        <source>Copy</source>
        <translation>Salin</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="90"/>
        <source>Cut</source>
        <translation>Potong</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="91"/>
        <source>Paste</source>
        <translation>Tampal</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="93"/>
        <source>Mark</source>
        <translation>Tanda</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="94"/>
        <source>Unmark</source>
        <translation>Nyahtanda</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="95"/>
        <source>Copy line</source>
        <translation>Salin baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="96"/>
        <source>Cut line</source>
        <translation>Potong baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="97"/>
        <source>Merge lines</source>
        <translation>Gabung baris</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="98"/>
        <source>Read-Only mode</source>
        <translation>Mod Baca-Sahaja</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="99"/>
        <source>Add comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="100"/>
        <source>Remove comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="101"/>
        <source>Undo</source>
        <translation>Buat Asal</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="102"/>
        <source>Redo</source>
        <translation>Buat Semula</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="103"/>
        <source>Add/Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="104"/>
        <source>Move to previous bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="105"/>
        <source>Move to next bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="106"/>
        <source>Advanced</source>
        <translation>Lanjutan</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="108"/>
        <source>Window size</source>
        <translation>Saiz tetingkap</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="112"/>
        <source>Code folding flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="113"/>
        <source>Show line numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="114"/>
        <source>Show bookmarks icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="115"/>
        <source>Show whitespaces and tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="116"/>
        <source>Highlight current line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="120"/>
        <source>Unicode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="121"/>
        <source>WesternEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="122"/>
        <source>CentralEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="123"/>
        <source>Baltic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="124"/>
        <source>Cyrillic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="125"/>
        <source>Arabic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="130"/>
        <source>Celtic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="131"/>
        <source>SouthEasternEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="126"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="127"/>
        <source>Hebrew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="132"/>
        <source>ChineseSimplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="133"/>
        <source>ChineseTraditional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="134"/>
        <source>Japanese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="135"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="129"/>
        <source>Thai</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="128"/>
        <source>Turkish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="136"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="117"/>
        <source>Color mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="176"/>
        <location filename="../src/editor/editwrapper.cpp" line="465"/>
        <location filename="../src/widgets/window.cpp" line="932"/>
        <location filename="../src/widgets/window.cpp" line="950"/>
        <location filename="../src/widgets/window.cpp" line="1027"/>
        <location filename="../src/widgets/window.cpp" line="1042"/>
        <source>Encoding</source>
        <translation type="unfinished">Pengekodan</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="951"/>
        <location filename="../src/widgets/window.cpp" line="1028"/>
        <location filename="../src/widgets/window.cpp" line="1043"/>
        <source>Line Endings</source>
        <translation type="unfinished">Penghujung Baris</translation>
    </message>
</context>
<context>
    <name>ReplaceBar</name>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="39"/>
        <source>Find</source>
        <translation>Cari</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="43"/>
        <source>Replace With</source>
        <translation>Ganti Dengan</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="47"/>
        <source>Replace</source>
        <translation>Ganti</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="50"/>
        <source>Skip</source>
        <translation>Langkau</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="53"/>
        <source>Replace Rest</source>
        <translation>Ganti Selebihnya</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="56"/>
        <source>Replace All</source>
        <translation>Ganti Semua</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/common/settings.cpp" line="106"/>
        <source>Standard</source>
        <translation>Piawai</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="106"/>
        <source>Customize</source>
        <translation>Suai</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Normal</source>
        <translation>Biasa</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Fullscreen</source>
        <translation>Skrin Penuh</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="337"/>
        <source>This shortcut conflicts with system shortcut %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="339"/>
        <source>This shortcut conflicts with %1, click on Replace to make this shortcut effective immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="454"/>
        <location filename="../src/common/settings.cpp" line="462"/>
        <source>The shortcut %1 is invalid, please set another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="497"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="498"/>
        <source>Replace</source>
        <translation>Ganti</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="500"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartManager</name>
    <message>
        <location filename="../src/startmanager.cpp" line="333"/>
        <source>Untitled %1</source>
        <translation type="unfinished">%1 Tidak Bertajuk</translation>
    </message>
</context>
<context>
    <name>Tabbar</name>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="468"/>
        <source>Close tab</source>
        <translation>Tutup tab</translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="470"/>
        <source>More options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="471"/>
        <source>Close tabs to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="472"/>
        <source>Close tabs to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="473"/>
        <source>Close unmodified tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="469"/>
        <source>Close other tabs</source>
        <translation>Tutup tab lain</translation>
    </message>
</context>
<context>
    <name>TextEdit</name>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="213"/>
        <source>Undo</source>
        <translation>Buat Asal</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="214"/>
        <source>Redo</source>
        <translation>Buat Semula</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="215"/>
        <source>Cut</source>
        <translation>Potong</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="216"/>
        <source>Copy</source>
        <translation>Salin</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="217"/>
        <source>Paste</source>
        <translation>Tampal</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="218"/>
        <source>Delete</source>
        <translation>Padam</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="219"/>
        <source>Select All</source>
        <translation>Pilih Semua</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="220"/>
        <location filename="../src/widgets/window.cpp" line="345"/>
        <source>Find</source>
        <translation>Cari</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="221"/>
        <location filename="../src/widgets/window.cpp" line="346"/>
        <source>Replace</source>
        <translation>Ganti</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="222"/>
        <source>Go to Line</source>
        <translation>Pergi ke Baris</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="223"/>
        <source>Turn on Read-Only mode</source>
        <translation>Hidupkan mod Baca-Sahaja</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="224"/>
        <source>Turn off Read-Only mode</source>
        <translation>Matikan mod Baca-Sahaja</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="225"/>
        <source>Fullscreen</source>
        <translation>Skrin Penuh</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="226"/>
        <source>Exit fullscreen</source>
        <translation>Keluar dari skrin penuh</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="227"/>
        <source>Display in file manager</source>
        <translation>Papar dalam pengurus fail</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="228"/>
        <location filename="../src/editor/dtextedit.cpp" line="286"/>
        <source>Add Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="233"/>
        <source>Column Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="287"/>
        <source>Remove Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="420"/>
        <source>Press ALT and click lines to edit in column mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="229"/>
        <source>Text to Speech</source>
        <translation>Teks ke Pertuturan</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="230"/>
        <source>Stop reading</source>
        <translation>Berhenti Membaca</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="231"/>
        <source>Speech to Text</source>
        <translation>Pertuturan ke Teks</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="232"/>
        <source>Translate</source>
        <translation>Terjemah</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="234"/>
        <source>Add bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="235"/>
        <source>Remove Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="236"/>
        <source>Previous bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="237"/>
        <source>Next bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="238"/>
        <source>Remove All Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="239"/>
        <source>Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="240"/>
        <source>Fold Current Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="241"/>
        <source>Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="242"/>
        <source>Unfold Current Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="247"/>
        <source>Color Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="280"/>
        <source>Mark All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="263"/>
        <source>Mark</source>
        <translation>Tanda</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="248"/>
        <source>Clear All Marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="249"/>
        <source>Clear Last Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="488"/>
        <source>Change Case</source>
        <translation>Ubah Kata</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="489"/>
        <source>Upper Case</source>
        <translation>Huruf Besar</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="490"/>
        <source>Lower Case</source>
        <translation>Huruf Kecil</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="491"/>
        <source>Capitalize</source>
        <translation>Huruf Besarkan</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="64"/>
        <source>None</source>
        <translation>Tiada</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1308"/>
        <source>Selected line(s) copied</source>
        <translation>Baris terpilih disalin</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1314"/>
        <source>Current line copied</source>
        <translation>Baris semasa disalin</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1353"/>
        <source>Selected line(s) clipped</source>
        <translation>Baris terpilih dikerat</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1359"/>
        <source>Current line clipped</source>
        <translation>Baris semasa dikerat</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="3097"/>
        <source>Read-Only mode is off</source>
        <translation>Mod Baca-Sahaja dimatikan</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="3103"/>
        <location filename="../src/editor/dtextedit.cpp" line="3111"/>
        <location filename="../src/editor/dtextedit.cpp" line="5328"/>
        <source>Read-Only mode is on</source>
        <translation>Mod Baca-Sahaja dihidupkan</translation>
    </message>
</context>
<context>
    <name>Toast</name>
    <message>
        <location filename="../src/controls/toast.cpp" line="38"/>
        <source>Reload</source>
        <translation>Muat Semula</translation>
    </message>
</context>
<context>
    <name>WarningNotices</name>
    <message>
        <location filename="../src/controls/warningnotices.cpp" line="15"/>
        <source>Reload</source>
        <translation>Muat Semula</translation>
    </message>
</context>
<context>
    <name>Window</name>
    <message>
        <location filename="../src/widgets/window.cpp" line="337"/>
        <source>New window</source>
        <translation>Tetingkap baharu</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="338"/>
        <source>New tab</source>
        <translation>Tab baharu</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="339"/>
        <source>Open file</source>
        <translation>Buka fail</translation>
    </message>
    <message>
        <location filename="../src/controls/toast.cpp" line="39"/>
        <location filename="../src/controls/warningnotices.cpp" line="17"/>
        <location filename="../src/widgets/window.cpp" line="341"/>
        <source>Save as</source>
        <translation>Simpan sebagai</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="342"/>
        <source>Print</source>
        <translation>Cetak</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="343"/>
        <source>Switch theme</source>
        <translation>Tukar tema</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="459"/>
        <location filename="../src/widgets/window.cpp" line="2074"/>
        <source>Read-Only</source>
        <translation>Baca-Sahaja</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="883"/>
        <source>Saved successfully</source>
        <translation>Berjaya disimpan</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1171"/>
        <source>Read-Only mode is on</source>
        <translation>Mod Baca-Sahaja dihidupkan</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1396"/>
        <source>Ctrl+&apos;=&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1399"/>
        <source>Ctrl+&apos;-&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1957"/>
        <source>Discard</source>
        <translation>Singkir</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="344"/>
        <location filename="../src/widgets/window.cpp" line="1446"/>
        <source>Settings</source>
        <translation>Tetapan</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="476"/>
        <source>You do not have permission to open %1</source>
        <translation>Anda tidak mempunyai keizinan untuk membuka %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="511"/>
        <source>Invalid file: %1</source>
        <translation>Fail tidak sah: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="930"/>
        <location filename="../src/widgets/window.cpp" line="984"/>
        <location filename="../src/widgets/window.cpp" line="1025"/>
        <source>Save File</source>
        <translation>Simpan Fail</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="614"/>
        <source>Do you want to save this file?</source>
        <translation>Anda mahu menyimpan fail ini?</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="986"/>
        <source>Encoding</source>
        <translation>Pengekodan</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1428"/>
        <source>Editor</source>
        <translation>Penyunting</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="866"/>
        <source>You do not have permission to save %1</source>
        <translation>Anda tidak mempunyai keizinan untuk menyimpan %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="577"/>
        <location filename="../src/widgets/window.cpp" line="899"/>
        <source>Do you want to save as another?</source>
        <translation>Anda mahu menyimpan sebagai yang lain?</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1656"/>
        <source>Untitled %1</source>
        <translation>%1 Tidak Bertajuk</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1345"/>
        <source>Current location remembered</source>
        <translation>Lokasi semasa diingati</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1956"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="340"/>
        <location filename="../src/widgets/window.cpp" line="1958"/>
        <source>Save</source>
        <translation>Simpan</translation>
    </message>
</context>
</TS>
