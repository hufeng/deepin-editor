<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar">
<context>
    <name>BottomBar</name>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="38"/>
        <source>Row</source>
        <translation>صف</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="39"/>
        <source>Column</source>
        <translation>عمود</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="40"/>
        <source>Characters %1</source>
        <translation>الحروف 1%</translation>
    </message>
</context>
<context>
    <name>DDropdownMenu</name>
    <message>
        <location filename="../src/widgets/ddropdownmenu.cpp" line="271"/>
        <location filename="../src/widgets/ddropdownmenu.cpp" line="321"/>
        <source>None</source>
        <translation type="unfinished">لا شيء</translation>
    </message>
</context>
<context>
    <name>EditWrapper</name>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="533"/>
        <source>File has changed on disk. Reload?</source>
        <translation>الملف الذي في القرص قد عُدِّل. أعيد التحميل؟</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="530"/>
        <source>File removed on the disk. Save it now?</source>
        <translation>الملف الذي في القرص قد حُذِف. أحفظه الآن؟</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="273"/>
        <source>Do you want to save this file?</source>
        <translation type="unfinished">هل تريد حفظ هذا الملف؟</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="231"/>
        <location filename="../src/editor/editwrapper.cpp" line="277"/>
        <source>Discard</source>
        <translation type="unfinished">إلغاء</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="227"/>
        <source>Encoding changed. Do you want to save the file now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="555"/>
        <location filename="../src/widgets/bottombar.cpp" line="60"/>
        <source>INSERT</source>
        <translation>إدخال</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="558"/>
        <source>OVERWRITE</source>
        <translation>إعادة الكتابة</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="561"/>
        <source>R/O</source>
        <translation>قراءة</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="230"/>
        <location filename="../src/editor/editwrapper.cpp" line="276"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="174"/>
        <location filename="../src/editor/editwrapper.cpp" line="232"/>
        <location filename="../src/editor/editwrapper.cpp" line="278"/>
        <location filename="../src/editor/editwrapper.cpp" line="463"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
</context>
<context>
    <name>FindBar</name>
    <message>
        <location filename="../src/controls/findbar.cpp" line="40"/>
        <source>Find</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../src/controls/findbar.cpp" line="46"/>
        <source>Next</source>
        <translation>التالي</translation>
    </message>
    <message>
        <location filename="../src/controls/findbar.cpp" line="44"/>
        <source>Previous</source>
        <translation>السابق</translation>
    </message>
</context>
<context>
    <name>JumpLineBar</name>
    <message>
        <location filename="../src/controls/jumplinebar.cpp" line="41"/>
        <source>Go to Line: </source>
        <translation>انتقل إلى السطر:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/editorapplication.cpp" line="7"/>
        <source>Text Editor is a powerful tool for viewing and editing text files.</source>
        <translation>محرر النصوص هو أداة قوية لعرض وتحرير الملفات النصية.</translation>
    </message>
    <message>
        <location filename="../src/editorapplication.cpp" line="18"/>
        <source>Text Editor</source>
        <translation>محرر نصي</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/startmanager.cpp" line="718"/>
        <source>File not saved</source>
        <translation>لم يتم حفظ الملف</translation>
    </message>
    <message>
        <location filename="../src/editorapplication.cpp" line="15"/>
        <source>Text Editor</source>
        <translation>محرر النصوص</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="24"/>
        <source>Basic</source>
        <translation>بسيط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="25"/>
        <source>Font Style</source>
        <translation>نوع الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="26"/>
        <source>Font</source>
        <translation>الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="27"/>
        <source>Font Size</source>
        <translation>حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="29"/>
        <location filename="../src/controls/settingsdialog.cpp" line="30"/>
        <source>Keymap</source>
        <translation>خارطة المفاتيح</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="28"/>
        <source>Shortcuts</source>
        <translation>الاختصارات</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="58"/>
        <source>Forward character</source>
        <translation>حرف للأمام</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="59"/>
        <source>Backward character</source>
        <translation>حرف للخلف</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="92"/>
        <source>Transpose character</source>
        <translation>تغيير مكان الحرف</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="109"/>
        <source>Tab width</source>
        <translation>عرض الشريط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="111"/>
        <source>Word wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="31"/>
        <location filename="../src/controls/settingsdialog.cpp" line="107"/>
        <location filename="../src/widgets/window.cpp" line="1386"/>
        <source>Window</source>
        <translation>النافذة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="32"/>
        <source>New tab</source>
        <translation>لسان جديد</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="33"/>
        <source>New window</source>
        <translation>نافذة جديدة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="34"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="35"/>
        <source>Save as</source>
        <translation>حفظ باسم</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="36"/>
        <source>Next tab</source>
        <translation>اللسان التالي</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="37"/>
        <source>Previous tab</source>
        <translation>اللسان السابق</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="38"/>
        <source>Close tab</source>
        <translation>أغلاق اللسان</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="39"/>
        <source>Close other tabs</source>
        <translation>أغلاق الألسنة الأخرى</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="40"/>
        <source>Restore tab</source>
        <translation>استعادة الألسنة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="41"/>
        <source>Open file</source>
        <translation>فتح الملف</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="42"/>
        <source>Increment font size</source>
        <translation>زد حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="43"/>
        <source>Decrement font size</source>
        <translation>أنقص حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="44"/>
        <source>Reset font size</source>
        <translation>إعادة تعيين حجم الخط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="46"/>
        <source>Toggle fullscreen</source>
        <translation>تبديل ملء الشاشة </translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="45"/>
        <source>Help</source>
        <translation>المساعدة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="47"/>
        <source>Find</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="48"/>
        <source>Replace</source>
        <translation>استبدال</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="49"/>
        <source>Go to line</source>
        <translation>انتقال إلى السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="50"/>
        <source>Save cursor position</source>
        <translation>حفظ موضع المؤشر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="51"/>
        <source>Reset cursor position</source>
        <translation>إعادة تعيين موضع المؤشر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="52"/>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="53"/>
        <source>Display shortcuts</source>
        <translation>عرض الاختصارات</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="54"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="55"/>
        <source>Editor</source>
        <translation>المحرر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="56"/>
        <source>Increase indent</source>
        <translation>زيادة المسافة البادئة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="57"/>
        <source>Decrease indent</source>
        <translation>نقصان المسافة البادئة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="60"/>
        <source>Forward word</source>
        <translation>كلمة إلى الأمام</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="61"/>
        <source>Backward word</source>
        <translation>كلمة إلى الخلف</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="62"/>
        <source>Next line</source>
        <translation>السطر التالي</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="63"/>
        <source>Previous line</source>
        <translation>السطر السابق</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="64"/>
        <source>New line</source>
        <translation>سطر جديد</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="65"/>
        <source>New line above</source>
        <translation>سطر جديد أعلى</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="66"/>
        <source>New line below</source>
        <translation>سطر جديد أسفل</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="67"/>
        <source>Duplicate line</source>
        <translation>مضاعفة السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="68"/>
        <source>Delete to end of line</source>
        <translation>حذف إلى نهاية السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="69"/>
        <source>Delete current line</source>
        <translation>حذف السطر الحالي</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="70"/>
        <source>Swap line up</source>
        <translation>بادل سطرًا للأعلى</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="71"/>
        <source>Swap line down</source>
        <translation>بادل سطرًا للأسفل</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="72"/>
        <source>Scroll up one line</source>
        <translation>انتقل لأعلى سطرًا واحدًا</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="73"/>
        <source>Scroll down one line</source>
        <translation>انتقل لأسفل سطرًا واحدًا</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="74"/>
        <source>Page up</source>
        <translation>صفحة لأعلى</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="75"/>
        <source>Page down</source>
        <translation>صفحة لأسفل</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="76"/>
        <source>Move to end of line</source>
        <translation>انتقل الى نهاية السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="77"/>
        <source>Move to start of line</source>
        <translation>انتقل الى بداية السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="78"/>
        <source>Move to end of text</source>
        <translation>انتقل إلى نهاية النص</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="79"/>
        <source>Move to start of text</source>
        <translation>انتقل إلى أول النص</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="80"/>
        <source>Move to line indentation</source>
        <translation>تحريك إلى سطر جديد</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="81"/>
        <source>Upper case</source>
        <translation>حرف كبير</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="82"/>
        <source>Lower case</source>
        <translation>حرف صغير</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="83"/>
        <source>Capitalize</source>
        <translation>تحويل الحروف إلى حروف كبيرة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="84"/>
        <source>Delete backward word</source>
        <translation>حذف الكلمة الخلفية</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="85"/>
        <source>Delete forward word</source>
        <translation>حذف الكلمة الأمامية</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="86"/>
        <source>Forward over a pair</source>
        <translation>تحويل إلى الأمام لأكثر من زوج</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="87"/>
        <source>Backward over a pair</source>
        <translation>تحويل إلى الخلف لأكثر من زوج</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="88"/>
        <source>Select all</source>
        <translation>اختر الكل</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="89"/>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="90"/>
        <source>Cut</source>
        <translation>قص</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="91"/>
        <source>Paste</source>
        <translation>لصق</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="93"/>
        <source>Mark</source>
        <translation>وضع علامة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="94"/>
        <source>Unmark</source>
        <translation>إزالة علامة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="95"/>
        <source>Copy line</source>
        <translation>نسخ السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="96"/>
        <source>Cut line</source>
        <translation>قص السطر</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="97"/>
        <source>Merge lines</source>
        <translation>دمج السطور</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="98"/>
        <source>Read-Only mode</source>
        <translation>وضع القراءة فقط</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="99"/>
        <source>Add comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="100"/>
        <source>Remove comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="101"/>
        <source>Undo</source>
        <translation>تراجع</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="102"/>
        <source>Redo</source>
        <translation>أعد</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="103"/>
        <source>Add/Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="104"/>
        <source>Move to previous bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="105"/>
        <source>Move to next bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="106"/>
        <source>Advanced</source>
        <translation>متقدم</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="108"/>
        <source>Window size</source>
        <translation>حجم النافذة</translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="112"/>
        <source>Code folding flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="113"/>
        <source>Show line numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="114"/>
        <source>Show bookmarks icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="115"/>
        <source>Show whitespaces and tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="116"/>
        <source>Highlight current line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="120"/>
        <source>Unicode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="121"/>
        <source>WesternEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="122"/>
        <source>CentralEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="123"/>
        <source>Baltic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="124"/>
        <source>Cyrillic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="125"/>
        <source>Arabic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="130"/>
        <source>Celtic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="131"/>
        <source>SouthEasternEuropean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="126"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="127"/>
        <source>Hebrew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="132"/>
        <source>ChineseSimplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="133"/>
        <source>ChineseTraditional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="134"/>
        <source>Japanese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="135"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="129"/>
        <source>Thai</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="128"/>
        <source>Turkish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="136"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/settingsdialog.cpp" line="117"/>
        <source>Color mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/editwrapper.cpp" line="176"/>
        <location filename="../src/editor/editwrapper.cpp" line="465"/>
        <location filename="../src/widgets/window.cpp" line="932"/>
        <location filename="../src/widgets/window.cpp" line="950"/>
        <location filename="../src/widgets/window.cpp" line="1027"/>
        <location filename="../src/widgets/window.cpp" line="1042"/>
        <source>Encoding</source>
        <translation type="unfinished">الترميز</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="951"/>
        <location filename="../src/widgets/window.cpp" line="1028"/>
        <location filename="../src/widgets/window.cpp" line="1043"/>
        <source>Line Endings</source>
        <translation type="unfinished">نهايات السطور</translation>
    </message>
</context>
<context>
    <name>ReplaceBar</name>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="39"/>
        <source>Find</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="43"/>
        <source>Replace With</source>
        <translation>استبدال بـ</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="47"/>
        <source>Replace</source>
        <translation>استبدال</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="50"/>
        <source>Skip</source>
        <translation>تخطي</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="53"/>
        <source>Replace Rest</source>
        <translation>استبدال الباقي</translation>
    </message>
    <message>
        <location filename="../src/controls/replacebar.cpp" line="56"/>
        <source>Replace All</source>
        <translation>استبدال الكل</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/common/settings.cpp" line="106"/>
        <source>Standard</source>
        <translation>قياسي</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="106"/>
        <source>Customize</source>
        <translation>معدل</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Normal</source>
        <translation>عادي</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Maximum</source>
        <translation>أقصى</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="120"/>
        <source>Fullscreen</source>
        <translation>ملء الشاشة</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="337"/>
        <source>This shortcut conflicts with system shortcut %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="339"/>
        <source>This shortcut conflicts with %1, click on Replace to make this shortcut effective immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="454"/>
        <location filename="../src/common/settings.cpp" line="462"/>
        <source>The shortcut %1 is invalid, please set another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="497"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="498"/>
        <source>Replace</source>
        <translation>استبدال</translation>
    </message>
    <message>
        <location filename="../src/common/settings.cpp" line="500"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartManager</name>
    <message>
        <location filename="../src/startmanager.cpp" line="333"/>
        <source>Untitled %1</source>
        <translation type="unfinished">بلا عنوان %1</translation>
    </message>
</context>
<context>
    <name>Tabbar</name>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="468"/>
        <source>Close tab</source>
        <translation>أغلق اللسان</translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="470"/>
        <source>More options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="471"/>
        <source>Close tabs to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="472"/>
        <source>Close tabs to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="473"/>
        <source>Close unmodified tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controls/tabbar.cpp" line="469"/>
        <source>Close other tabs</source>
        <translation>أغلق الألسنة الأخرى</translation>
    </message>
</context>
<context>
    <name>TextEdit</name>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="213"/>
        <source>Undo</source>
        <translation>تراجع</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="214"/>
        <source>Redo</source>
        <translation>أعد</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="215"/>
        <source>Cut</source>
        <translation>قص</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="216"/>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="217"/>
        <source>Paste</source>
        <translation>لصق</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="218"/>
        <source>Delete</source>
        <translation>حذف</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="219"/>
        <source>Select All</source>
        <translation>اختر الكل</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="220"/>
        <location filename="../src/widgets/window.cpp" line="345"/>
        <source>Find</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="221"/>
        <location filename="../src/widgets/window.cpp" line="346"/>
        <source>Replace</source>
        <translation>استبدال</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="222"/>
        <source>Go to Line</source>
        <translation>انتقال إلى سطر</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="223"/>
        <source>Turn on Read-Only mode</source>
        <translation>تشغيل وضع القراءة فقط</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="224"/>
        <source>Turn off Read-Only mode</source>
        <translation>ايقاف وضع القراءة فقط</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="225"/>
        <source>Fullscreen</source>
        <translation>ملء الشاشة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="226"/>
        <source>Exit fullscreen</source>
        <translation>الخروج من وضع ملء للشاشة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="227"/>
        <source>Display in file manager</source>
        <translation>عرض في مدير الملفات</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="228"/>
        <location filename="../src/editor/dtextedit.cpp" line="286"/>
        <source>Add Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="233"/>
        <source>Column Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="287"/>
        <source>Remove Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="420"/>
        <source>Press ALT and click lines to edit in column mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="229"/>
        <source>Text to Speech</source>
        <translation>النص إلى الكلام</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="230"/>
        <source>Stop reading</source>
        <translation>توقف عن القراءة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="231"/>
        <source>Speech to Text</source>
        <translation>الكلام إلى النص</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="232"/>
        <source>Translate</source>
        <translation>تَرجم</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="234"/>
        <source>Add bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="235"/>
        <source>Remove Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="236"/>
        <source>Previous bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="237"/>
        <source>Next bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="238"/>
        <source>Remove All Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="239"/>
        <source>Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="240"/>
        <source>Fold Current Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="241"/>
        <source>Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="242"/>
        <source>Unfold Current Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="247"/>
        <source>Color Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="280"/>
        <source>Mark All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="263"/>
        <source>Mark</source>
        <translation>وضع علامة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="248"/>
        <source>Clear All Marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="249"/>
        <source>Clear Last Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="488"/>
        <source>Change Case</source>
        <translation>غيِّر الخانة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="489"/>
        <source>Upper Case</source>
        <translation>احرف الكبيرة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="490"/>
        <source>Lower Case</source>
        <translation>أحرف صغيرة</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="491"/>
        <source>Capitalize</source>
        <translation>تحويل الحروف إلى حروف كبيرة</translation>
    </message>
    <message>
        <location filename="../src/widgets/bottombar.cpp" line="64"/>
        <source>None</source>
        <translation>لا شيء</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1308"/>
        <source>Selected line(s) copied</source>
        <translation>تم نسخ الخط (الخطوط) المحدد</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1314"/>
        <source>Current line copied</source>
        <translation>تم نسخ السطر الحالي</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1353"/>
        <source>Selected line(s) clipped</source>
        <translation>تم قص الخط (الخطوط) المحدد</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="1359"/>
        <source>Current line clipped</source>
        <translation>تم قص الخط المحدد</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="3097"/>
        <source>Read-Only mode is off</source>
        <translation>وضع القراءة فقط متوقف</translation>
    </message>
    <message>
        <location filename="../src/editor/dtextedit.cpp" line="3103"/>
        <location filename="../src/editor/dtextedit.cpp" line="3111"/>
        <location filename="../src/editor/dtextedit.cpp" line="5328"/>
        <source>Read-Only mode is on</source>
        <translation>وضع القراءة فقط قيد التشغيل</translation>
    </message>
</context>
<context>
    <name>Toast</name>
    <message>
        <location filename="../src/controls/toast.cpp" line="38"/>
        <source>Reload</source>
        <translation>أعد التحميل</translation>
    </message>
</context>
<context>
    <name>WarningNotices</name>
    <message>
        <location filename="../src/controls/warningnotices.cpp" line="15"/>
        <source>Reload</source>
        <translation>أعد التحميل</translation>
    </message>
</context>
<context>
    <name>Window</name>
    <message>
        <location filename="../src/widgets/window.cpp" line="337"/>
        <source>New window</source>
        <translation>نافذة جديدة</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="338"/>
        <source>New tab</source>
        <translation>لسان جديد</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="339"/>
        <source>Open file</source>
        <translation>فتح الملف</translation>
    </message>
    <message>
        <location filename="../src/controls/toast.cpp" line="39"/>
        <location filename="../src/controls/warningnotices.cpp" line="17"/>
        <location filename="../src/widgets/window.cpp" line="341"/>
        <source>Save as</source>
        <translation>حفظ باسم</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="342"/>
        <source>Print</source>
        <translation>طباعة</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="343"/>
        <source>Switch theme</source>
        <translation>غيِّر السمة</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="459"/>
        <location filename="../src/widgets/window.cpp" line="2074"/>
        <source>Read-Only</source>
        <translation>للقراءة فقط</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="883"/>
        <source>Saved successfully</source>
        <translation>حُفِظ بنجاح</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1171"/>
        <source>Read-Only mode is on</source>
        <translation>وضع القراءة فقط قيد التشغيل</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1396"/>
        <source>Ctrl+&apos;=&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1399"/>
        <source>Ctrl+&apos;-&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1957"/>
        <source>Discard</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="344"/>
        <location filename="../src/widgets/window.cpp" line="1446"/>
        <source>Settings</source>
        <translation>إعدادات</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="476"/>
        <source>You do not have permission to open %1</source>
        <translation>لا تملك الإذن لفتح 1%</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="511"/>
        <source>Invalid file: %1</source>
        <translation>ملف غير مدعوم: 1%</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="930"/>
        <location filename="../src/widgets/window.cpp" line="984"/>
        <location filename="../src/widgets/window.cpp" line="1025"/>
        <source>Save File</source>
        <translation>حفظ الملف</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="614"/>
        <source>Do you want to save this file?</source>
        <translation>هل تريد حفظ هذا الملف؟</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="986"/>
        <source>Encoding</source>
        <translation>الترميز</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1428"/>
        <source>Editor</source>
        <translation>المحرر</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="866"/>
        <source>You do not have permission to save %1</source>
        <translation>لا تملك الإذن لحفظ 1%</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="577"/>
        <location filename="../src/widgets/window.cpp" line="899"/>
        <source>Do you want to save as another?</source>
        <translation>أتريد حفظه باسم آخر؟</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1656"/>
        <source>Untitled %1</source>
        <translation>بلا عنوان %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1345"/>
        <source>Current location remembered</source>
        <translation>تم تذكر المكان الحالي</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="1956"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../src/widgets/window.cpp" line="340"/>
        <location filename="../src/widgets/window.cpp" line="1958"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
</context>
</TS>
